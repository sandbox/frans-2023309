<?php
/**
 * Feeds parser plugin that parses OPML feeds.
 */
class FeedsGoogleSpreadsheetParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {

    $config = $source->getConfigFor($this);

    $item_key = $config['items_are'];
    $value_key = 'col';
    if ($item_key == 'col') {
      $value_key = 'row';
    }
    $xml = @ new SimpleXMLElement($fetcher_result->getRaw());
    $cells = $xml->xpath('//gs:cell');
    $results = array();
    foreach ($cells as $cell) {
      $item = (int)$cell->attributes()->{$item_key};
      $value = (int)$cell->attributes()->{$value_key};
      $results[$item][$value] = trim((string)$cell);
    }
    $headers = array();
    if ($config['header_position'] > 0) {
      $headers = $results[$config['header_position']];
    }
    for($i=0;$i<=$config['skip_first_items'];$i++) {
      unset($results[$i]);
    }

    // Assign headers.
    if (count($headers)) {
      foreach ($results as $i => $vals) {
        foreach($headers as $key => $header_name) {
          if (isset($results[$i][$key])) {
            $results[$i][$header_name] = $results[$i][$key];
          }
        }
      }
    }

    // Weed out the ones we need to weed out.
    foreach ($results as $i => $vals) {
      if (strlen($config['skip_without_header'])) {
        if (!isset($results[$i][$config['skip_without_header']]) || !strlen($results[$i][$config['skip_without_header']])) {
          unset($results[$i]);
        }
      }
      if (strlen($config['skip_with_header'])) {
        if (isset($results[$i][$config['skip_with_header']]) && strlen($results[$i][$config['skip_with_header']])) {
          unset($results[$i]);
        }
      }
    }

    // Reset array.
    ksort($results, SORT_NUMERIC);
    $results = array_values($results);

    $result = new FeedsParserResult($results);
    $result->title = t('Google Spreadsheet');
    return $result;
  }

  /**
   * Define defaults.
   */
  public function sourceDefaults() {
    return array(
      'header_position' => $this->config['header_position'],
      'skip_first_items' => $this->config['skip_first_items'],
      'items_are' => $this->config['items_are'],
      'skip_with_header' => $this->config['skip_with_header'],
      'skip_without_header' => $this->config['skip_without_header'],
    );
  }

  /**
   * Define default configuration.
   */
  public function configDefaults() {
    return array(
      'header_position' => '-1',
      'skip_first_items' => '-1',
      'items_are' => 'row',
      'skip_with_header' => '',
      'skip_without_header' => '',
    );
  }

  public function sourceForm($source_config) {
    $form_state = array();
    $form = $this->configForm($form_state);
    foreach ($source_config as $key => $value) {
      $form[$key]['#default_value'] = $value;
    }
    return $form;
  }

  /**
   * Build configuration form.
   */
  public function configForm(&$form_state) {
    $form = array();
    $form['items_are'] = array(
      '#type' => 'select',
      '#title' => t('Items are'),
      '#description' => t('Is this a vertical or a horizontal spreadsheet?'),
      '#options' => array(
        'row' => t('Rows'),
        'col' => t('Columns'),
      ),
      '#default_value' => $this->config['items_are'],
    );
    $form['header_position'] = array(
      '#type' => 'select',
      '#title' => t('Header position.'),
      '#description' => t('On what row or column is the header located?'),
      '#options' => array(
        '-1' => t('No header'),
        '1' => t('1st item'),
        '2' => t('2nd item'),
        '3' => t('3rd item'),
        '4' => t('4th item'),
        '5' => t('5th item'),
      ),
      '#default_value' => $this->config['header_position'],
    );
    $form['skip_first_items'] = array(
      '#type' => 'select',
      '#title' => t('Skip first items.'),
      '#description' => t('How many rows or columns should be skipped?'),
      '#options' => array(
        '-1' => t('None'),
        '1' => t('1 item'),
        '2' => t('2 items'),
        '3' => t('3 items'),
        '4' => t('4 items'),
        '5' => t('5 items'),
        '6' => t('6 items'),
        '7' => t('7 items'),
        '8' => t('8 items'),
        '9' => t('9 items'),
        '10' => t('10 items'),
      ),
      '#default_value' => $this->config['skip_first_items'],
    );
    $form['skip_with_header'] = array(
      '#type' => 'textfield',
      '#title' => t('Skip item when this field has a value.'),
      '#description' => t('When this field has a value, the complete item will be skipped.'),
      '#default_value' => $this->config['skip_with_header'],
    );
    $form['skip_without_header'] = array(
      '#type' => 'textfield',
      '#title' => t('Skip item when this field has no value.'),
      '#description' => t('When this field has no value, the complete item will be skipped.'),
      '#default_value' => $this->config['skip_without_header'],
    );
    return $form;
  }

}