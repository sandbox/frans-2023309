<?php

/**
 * Result of FeedsHTTPFetcher::fetch().
 */
class FeedsOAuthFetcherResult extends FeedsFetcherResult {

  protected $url;
  protected $file_path;
  protected $timeout;
  protected $consumer;

  /**
   * Constructor.
   */
  public function __construct($url = NULL) {
    $this->url = $url;
    parent::__construct('');
  }

  /**
   * Overrides FeedsFetcherResult::getRaw();
   */
  public function getRaw() {
    try {
      $access_token = unserialize($_SESSION['GO_' . $this->getConsumer() . '_ACCESS_TOKEN']);
      $client = guzzle_oauth_get_client_by_instance($this->getConsumer(), $access_token);
      $request = $client->get($this->url)->send();
    } catch(Exception $e) {
      throw new Exception(t('Could not download @url', array('@url' => $this->url)));
    }
    if (!in_array($request->getStatusCode(), array(200, 201, 202, 203, 204, 205, 206))) {
      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $request->getStatusCode())));
    }
    return $this->sanitizeRaw((string)$request->getBody());
  }

  public function getTimeout() {
    return $this->timeout;
  }

  public function setTimeout($timeout) {
    $this->timeout = $timeout;
  }

  public function getConsumer() {
    return $this->consumer;
  }

  public function setConsumer($consumer) {
    $this->consumer = $consumer;
  }

}

class FeedsOAuthFetcher extends FeedsHTTPFetcher {

  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $fetcher_result = new FeedsOAuthFetcherResult($source_config['source']);
    $fetcher_result->setTimeout($this->config['request_timeout']);
    $fetcher_result->setConsumer($this->config['consumer']);
    return $fetcher_result;
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    $config = parent::configDefaults();
    $config['consumer'] = NULL;
    return $config;
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    unset($form['auto_detect_feeds'], $form['use_pubsubhubbub'], $form['designated_hub']);

    $options = array();
    $instances = db_select('guzzle_oauth_consumer_instance', 'c')
      ->fields('c', array('id', 'title'))
      ->condition('status', 1)
      ->execute();
    foreach ($instances as $instance) {
      $options[$instance->id] = check_plain($instance->title);
    }
    $form['consumer'] = array(
      '#type' => 'select',
      '#title' => t('Consumer'),
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => $this->config['consumer'],
    );
    return $form;
  }


  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('Endpoint'),
      '#description' => t('Enter a endpoint URL.'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    return $form;
  }
}
